﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Runtime.CompilerServices;
using System.Text;

namespace msg_tsp
{
    class Office
    {
        public int ID { get; set; }
        public String City { get; set; }
        public GeoCoordinate Coordinate { get; set; }
    }
}
