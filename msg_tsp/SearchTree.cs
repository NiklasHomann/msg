﻿using Microsoft.Win32.SafeHandles;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace msg_tsp
{
    class SearchTree
    {
        public Solution StartSearch(double[,] distances, int count)
        {
            var start = DateTime.Now;

            Solution startSolution = Solution.GetInitialSolution(distances, count);
            Solution bestSolution = Search(null, startSolution);

            var dif = DateTime.Now.Subtract(start).TotalSeconds;
            Console.WriteLine(dif);

            return bestSolution;
        }

        Solution Search(Solution bestSolution, Solution currentSolution)
        {
            if (currentSolution.UnvisitedOffices.Count == 0)
            {
                if ((bestSolution == null) || (currentSolution.Length < bestSolution.Length))
                    return currentSolution;
                else
                    return null;
            }

            if (bestSolution != null && currentSolution.GetLowerBound() >= bestSolution.Length)
                return null;

            bool betterFound = false;
            foreach (int nextOffice in currentSolution.UnvisitedOffices) //explore all possibilites "best" first
            {
                Solution temp = Search(bestSolution, new Solution(currentSolution, nextOffice));
                if (temp != null)
                {
                    bestSolution = temp;
                    betterFound = true;
                }
            }

            return betterFound ? bestSolution : null;
        }
    }


   
}
