﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.IO;

namespace msg_tsp
{
    class Program
    {
        static void Main(string[] args)
        {
            var offices = GetOffices();
            double[,] distances = GetDistances(offices);
            Solution solution = new SearchTree().StartSearch(distances,offices.Count);
            Console.WriteLine(solution);
            Console.ReadLine();
        }

        static List<Office> GetOffices()
        {
            List<Office> offices = new List<Office>();
            using (StreamReader sr = new StreamReader("msg_standorte_deutschland.csv"))
            {
                sr.ReadLine(); //ignore first line
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var splittedLine = line.Split(',');
                    offices.Add(new Office() { ID = int.Parse(splittedLine[0]), City = splittedLine[1], Coordinate = new GeoCoordinate(double.Parse(splittedLine[6], CultureInfo.InvariantCulture),double.Parse(splittedLine[7], CultureInfo.InvariantCulture)) });
                }
            }
            return offices;
        }

        static double[,] GetDistances(List<Office> offices)
        {
            var distances = new double[offices.Count, offices.Count];
            for (int i = 0; i < offices.Count; i++)
            {
                for (int j = 0; j < offices.Count; j++)
                {
                    distances[i, j] = offices[i].Coordinate.GetDistanceTo(offices[j].Coordinate);
                }
            }
            return distances;
        }
    }
}
